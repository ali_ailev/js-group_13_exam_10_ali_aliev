const fs = require('fs').promises;
const {nanoid} = require("nanoid");

const filename = './commentDb.json';

let commentData = [];

module.exports = {
    async init() {
        try {
            const fileContents = await fs.readFile(filename);
            commentData = JSON.parse(fileContents.toString());
        } catch (e) {
            commentData = [];
        }
    },
    getItems() {
        return commentData;
    },
    getItem(id) {
        return commentData.find(p => p.id === id);
    },
    addItem(item) {
        item.id = nanoid();
        commentData.push(item);
        return this.save();
    },
    save() {
        return fs.writeFile(filename, JSON.stringify(commentData));
    },

    async deleteItem(id) {
        await this.delete();
        return  commentData.filter((p, index) =>{
            if(p.id === id) {
                commentData.splice(index,1);
            }
        });
    },

    async delete() {
        await fs.unlink(filename);
    },
};