const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('./config');
const db = require('./commentDb');

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

router.get('/', (req, res) => {
    const comments = db.getItems();
    return res.send(comments);
});

router.post('/', upload.single(), async (req, res, next) => {
    try {
        if (!req.body.title || !req.body.content) {
            return res.status(400).send({message: 'Title or content are required'});
        }

        const comments = {
            title: req.body.title,
            content: req.body.content,
            news_id: req.body.news_id,
        };

        await db.addItem(comments);

        return res.send(comments);
    } catch (e) {
        next(e);
    }
});

router.delete('/:id', async (req, res) => {
    const oneComments= db.getItem(req.params.id);
    if (!oneComments) {
        return res.status(404).send({message: 'Not found'});
    }
    await db.deleteItem(req.params.id);
    await db.save();
    return res.send("Comment with id " + req.params.id + " has been deleted");
});

module.exports = router;