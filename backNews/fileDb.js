const fs = require('fs').promises;
const {nanoid} = require("nanoid");

const filename = './db.json';
const filename2 = './commentDb.json'
let data = [];
let commentData = [];

module.exports = {
    async init() {
        try {
            const fileContents = await fs.readFile(filename);
            data = JSON.parse(fileContents.toString());
            const fileContents2 = await fs.readFile(filename2);
            commentData = JSON.parse(fileContents2.toString());
        } catch (e) {
            data = [];
            commentData = [];
        }
    },
    getItems() {
        return data;
    },
    getItem(id) {
        return data.find(p => p.id === id);
    },
    addItem(item) {
        item.id = nanoid();
        data.push(item);
        return this.save();
    },
    save() {
        fs.writeFile(filename2, JSON.stringify(commentData, null, 2));
        return fs.writeFile(filename, JSON.stringify(data, null, 2));
    },

    async deleteItem(id) {
        await this.delete();
        commentData.filter((p,index) => {
            if(p.news_id === id) {
                commentData.splice(index,1);
            }
        })
        return  data.filter((p, index) =>{
            if(p.id === id) {
                data.splice(index,1);
            }
        });
    },

   async delete() {
        await fs.unlink(filename);
        await fs.unlink(filename2);
    },
};