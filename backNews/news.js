const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('./config');
const db = require('./fileDb');

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

router.get('/', (req, res) => {
    const news = db.getItems();
    return res.send(news);
});

router.get('/:id', (req, res) => {
    const oneNews = db.getItem(req.params.id);

    if (!oneNews) {
        return res.status(404).send({message: 'Not found'});
    }

    return res.send(oneNews);
});

router.post('/', upload.single('image'), async (req, res, next) => {
    try {
        const date = new Date();
        if (!req.body.title || !req.body.content) {
            return res.status(400).send({message: 'Title or content are required'});
        }

        const news = {
            title: req.body.title,
            content: req.body.content,
            date: date,
        };

        if (req.file) {
            news.image = req.file.filename;
        }

        await db.addItem(news);

        return res.send(news);
    } catch (e) {
        next(e);
    }
});

router.delete('/:id', async (req, res) => {
    const oneNews = db.getItem(req.params.id);
    if (!oneNews) {
        return res.status(404).send({message: 'Not found'});
    }
    await db.deleteItem(req.params.id);
    await db.save();
    return res.send("News with id " + req.params.id + " has been deleted");
});

module.exports = router;