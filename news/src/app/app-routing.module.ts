import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PostsComponent } from './pages/posts/posts.component';
import { EditPostComponent } from './pages/edit-post/edit-post.component';
import { PostComponent } from './pages/posts/post/post.component';

const routes: Routes = [
  {path: '', component: PostsComponent},
  {path: 'posts/new', component: EditPostComponent},
  {path: 'posts/:id', component: PostComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
