export class CommentModel {
  constructor(
    public title: string,
    public content: string,
    public news_id: string,
    public id: string,
  ) {
  }
}

export interface CommentData {
  title: string;
  content: string;
  news_id: string;
  id: string;
}
