export class NewsModel {
  constructor(
    public title: string,
    public content: string,
    public date: string,
    public image: string,
    public id: string,
  ) {
  }
}

export interface NewsData {
  title: string;
  content: string;
  date: string;
  image: File | null;
  id: string;
}
