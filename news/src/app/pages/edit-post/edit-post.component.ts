import { Component, OnInit, ViewChild } from '@angular/core';
import { NewsServiceService } from '../../services/news-service.service';
import { NewsData } from '../../models/news.model';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-post',
  templateUrl: './edit-post.component.html',
  styleUrls: ['./edit-post.component.sass']
})
export class EditPostComponent implements OnInit {
  @ViewChild('f') form!: NgForm;

  constructor(
    private newsService: NewsServiceService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
  }

  onSubmit() {
    const newsData: NewsData = this.form.value;
    this.newsService.createNews(newsData).subscribe(() => {
      void this.router.navigate(['/']);
    });
  }

}
