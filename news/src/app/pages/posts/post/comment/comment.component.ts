import { Component, OnInit } from '@angular/core';
import { CommentService } from '../../../../services/comment.service';
import { CommentData, CommentModel } from '../../../../models/comment.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.sass']
})
export class CommentComponent implements OnInit {
  comments: CommentModel[] = [];
  id:string = '';

  constructor(private commentService: CommentService,
              private activateRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.id = this.activateRoute.snapshot.params['id'];
    this.commentService.getComments().subscribe(comments => {
      this.comments = comments.filter(p => p.news_id === this.id);
    });
  }

  onDelete(commentData: CommentData) {
    this.commentService.deleteComment(commentData).subscribe();
    window.location.reload();
  }

}
