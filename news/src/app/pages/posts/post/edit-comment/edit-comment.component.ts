import { Component, OnInit, ViewChild } from '@angular/core';
import { CommentService } from '../../../../services/comment.service';
import { NgForm } from '@angular/forms';
import { CommentData } from '../../../../models/comment.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-comment',
  templateUrl: './edit-comment.component.html',
  styleUrls: ['./edit-comment.component.sass']
})
export class EditCommentComponent implements OnInit {
  @ViewChild('g') form!: NgForm;

  constructor(
    private commentService: CommentService,
    private activateRoute: ActivatedRoute,
  ) {
  }

  ngOnInit(): void {
  }

  onSubmit() {
    if(this.form.value.title === '') {
      this.form.value.title = "Anonymous";
    }
    const commentData: CommentData = this.form.value;
    commentData.news_id = this.activateRoute.snapshot.params['id'];
    console.log(commentData);
    this.commentService.createComment(commentData).subscribe(() => {
      window.location.reload();
    });
  }
}
