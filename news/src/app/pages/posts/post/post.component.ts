import { Component, OnInit } from '@angular/core';
import { NewsModel } from '../../../models/news.model';
import { ActivatedRoute } from '@angular/router';
import { NewsServiceService } from '../../../services/news-service.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.sass']
})
export class PostComponent implements OnInit {
  id: string = '';

  constructor(private activateRoute: ActivatedRoute,
              private newsService: NewsServiceService) {
  }

  newses: NewsModel[] = [];


  ngOnInit(): void {
    this.id = this.activateRoute.snapshot.params['id'];
    this.newsService.getNews().subscribe(newses => {
      this.newses = newses.filter(p => p.id === this.id);
    });
  }


}
