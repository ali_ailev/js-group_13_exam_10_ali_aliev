import { Component, OnInit } from '@angular/core';
import { NewsModel } from '../../models/news.model';
import { NewsServiceService } from '../../services/news-service.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.sass']
})
export class PostsComponent implements OnInit {
  newses: NewsModel[] = [];

  constructor(
    private newsService: NewsServiceService
  ) {
  }

  ngOnInit(): void {
    this.newsService.getNews().subscribe(newses => {
      this.newses = newses;
      console.log(this.newses);
    });
  }

  onDelete(newsData: NewsModel) {
    this.newsService.deleteNews(newsData).subscribe();
    window.location.reload();
  }

}
