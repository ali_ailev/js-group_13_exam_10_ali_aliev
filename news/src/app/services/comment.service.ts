import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';
import { CommentData, CommentModel } from '../models/comment.model';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  constructor(private http: HttpClient) {
  }

  getComments() {
    return this.http.get<CommentModel[]>(environment.apiUrl + '/comments').pipe(
      map(response => {
        return response.map(commentData => {
          return new CommentModel(
            commentData.title,
            commentData.content,
            commentData.news_id,
            commentData.id,
          );
        });
      })
    )
  }

  createComment(commentData: CommentData) {
    const formData = new FormData();
    formData.append('title', commentData.title);
    formData.append('content', commentData.content);
    formData.append('news_id', commentData.news_id);

    console.log(formData.get('news_id'));
    return this.http.post(environment.apiUrl + '/comments', formData);
  }

  deleteComment(commentData: CommentData) {
    const formData = new FormData();
    formData.append('title', commentData.title);
    formData.append('content', commentData.content);
    formData.append('news_id', commentData.news_id);
    formData.append('id', commentData.id);

    return this.http.delete(environment.apiUrl + '/comments/' + commentData.id);
  }
}
