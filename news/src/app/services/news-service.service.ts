import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { NewsData, NewsModel } from '../models/news.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NewsServiceService {

  constructor(private http: HttpClient) {
  }

  getNews() {
    return this.http.get<NewsModel[]>(environment.apiUrl + '/news').pipe(
      map(response => {
        return response.map(newsData => {
          return new NewsModel(
            newsData.title,
            newsData.content,
            newsData.date,
            newsData.image,
            newsData.id,
          );
        });
      })
    )
  }

  createNews(newsData: NewsData) {
    const formData = new FormData();
    formData.append('title', newsData.title);
    formData.append('content', newsData.content);

    if (newsData.image) {
      formData.append('image', newsData.image);
    }

    return this.http.post(environment.apiUrl + '/news', formData);
  }

  deleteNews(newsData: NewsModel) {
    const formData = new FormData();
    formData.append('title', newsData.title);
    formData.append('content', newsData.content);

    if (newsData.image) {
      formData.append('image', newsData.image);
    }
    formData.append('id', newsData.id);

    return this.http.delete(environment.apiUrl + '/news/' + newsData.id);
  }
}
